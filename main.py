__version__ = "1.0"

import kivy
kivy.require('1.9.0')

from kivy.config import Config
Config.set('graphics', 'width', '720')
Config.set('graphics', 'height', '1280')

from kivy.app import App
from kivy.core.text import LabelBase

from kivy.core.window import Window
Window.softinput_mode = 'pan'

from kivy.utils import get_color_from_hex
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.storage.jsonstore import JsonStore
from hashlib import sha256
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from kivy.uix.image import Image, AsyncImage

import os
import stego
import imghdr
import requests
import json
from PIL import Image as Img
import string
import shutil

store = JsonStore('account.json')
global global_user
global_user = None
global global_image_path
global_image_path = None

def check_printable(str):
	letter = string.printable[:-5]
	#print letter
	if all(i in letter for i in str):
		return str
	else:
		return 'Your key is Wrong'

def hash(string):
	return sha256(string).hexdigest()



def send(path, sender, receiver):
	try:
		s = requests.Session()
		url = 'https://senare.herokuapp.com/image/'
		files = {'image': open(path, 'rb')}
		re = s.post(url + 'upload', files=files)
		status = json.loads(re.text)
		print 'Send Status', status, type(status['error'])
		if not status['error']:
			params = {'name':status['name'],'sender': sender, 'receiver': receiver}
			re = s.get(url+'uploadcomplete', params=params)
			print re.text, 'Send file Succesful'
			return True
	except Exception as e:
		print 'Network Error Send'
		print e.message
		url = 'http://senare-lenuage.rhcloud.com/image/'
		print 'Switch to: ', url
		files = {'image': open(path, 'rb')}
		re = requests.post(url + 'upload', files=files)
		status = json.loads(re.text)
		print 'Send Status', status, type(status['error'])
		if not status['error']:
			params = {'name':status['name'],'sender': sender, 'receiver': receiver}
			re = requests.get(url+'uploadcomplete', params=params)
			print re.text, 'Send file Succesful'
			return True

		return False

def getfile(sender, receiver):
	try:
		s = requests.Session()
		url = 'https://senare.herokuapp.com/image/'
		params = {'sender': sender, 'receiver': receiver}
		re = s.get(url+'geturls', params=params)
		print re.text
		status =  json.loads(re.text)
		if not status['error']:
			print status, status['urls'][0]
			filename = status['urls'][0]
			print filename[-16:]
			image = s.get(filename, stream=True)
			with open(filename[-16:], 'wb') as out_file:
				shutil.copyfileobj(image.raw, out_file)
			del image
			print 'Get file Done'
			s.get(url + 'downloadcomplete',params=params)
			return filename[-16:]
		else:
			print 'Error is ',status['message']
			return False
	except IndexError:
		print 'Network error'
		url = 'http://senare-lenuage.rhcloud.com/image/'
		print 'Switch to: ', url
		params = {'sender': sender, 'receiver': receiver}
		re = requests.get(url+'geturls', params=params)
		print re.text
		status =  json.loads(re.text)
		if not status['error']:
			print status, status['urls'][0]
			filename = status['urls'][0]
			print filename[-16:]
			image = requests.get(filename, stream=True)
			with open(filename[-16:], 'wb') as out_file:
				shutil.copyfileobj(image.raw, out_file)
			del image
			print 'Get file Done'
			requests.get(url + 'downloadcomplete',params=params)
			return filename[-16:]
		else:
			print 'Error is ',status['message']
			return False
		return False

class ProgramRoot(ScreenManager):
	pass

class Login(Screen):
	account = ObjectProperty()
	password = ObjectProperty()
	warning = ObjectProperty()
	store = JsonStore('account.json')

	def check(self):
		if self.account.text and self.password.text:
			user = hash(self.account.text)
			password = hash(self.password.text)
			if store.exists(user):
				pw = store.get(user)['Password']
				if password == pw:
					self.warning.text = 'Succesful'
					global global_user
					global_user = self.account.text
					return True
				else:
					self.warning.text = 'Wrong Password'
			else:
				self.warning.text = 'Username is not exists'
					
		else:
			self.warning.text = 'Please input Username or Password'

class CreateNewAccount(Screen):
	account = ObjectProperty()
	password = ObjectProperty()
	password_confirm = ObjectProperty()
	warning = ObjectProperty()

	def create_new_account(self):
		if self.account.text and self.password.text \
			and self.password_confirm.text:
			if self.password.text != self.password_confirm.text:
				self.warning.text = 'Password type mismatch'
			else:
				user = hash(self.account.text)
				password = hash(self.password.text)
				if store.exists(user) and password == store.get(user)['Password']:
					self.warning.text = 'This account is exists'
				else:
					store.put(hash(self.account.text), Password=hash(self.password.text))
					self.warning.text = '"%s" account has been created...' %self.account.text
					warning = 'Please login use the account you"ve created'
					return True
		else:
			self.warning.text = 'Please input all fields'

class LoadDialog(FloatLayout):
	load = ObjectProperty(None)
	cancel = ObjectProperty(None)


class Send(Screen):
	loadfile = ObjectProperty(None)
	savefile = ObjectProperty(None)
	image_file = None
	image_path = None
	warning = ObjectProperty()
	message = ObjectProperty()
	receiver = ObjectProperty()
	password = ObjectProperty()

	def dismiss_popup(self):
		self._popup.dismiss()

	def show_load(self):
		content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
		self._popup = Popup(title="Load Image", content=content,
							size_hint=(0.9, 0.9))
		self._popup.open()

	def load(self, path, filename):
		try:
			with open(os.path.join(path, filename[0])) as stream:
				self.image_file = stream
				print stream
				global global_image_path
				global_image_path = self.image_path = os.path.join(path, filename[0])
				print self.image_path
				#self.image_file = True
		except IndexError:
			self.warning.text = "You've not choose yet"

		self.ids.displayimage.source = self.image_path
		
		self.dismiss_popup()

	def encrypt(self):
		if not self.image_file and self.message.text: 		#when image is not select
			self.warning.text = 'Please choose an Image'

		elif not self.message.text and self.image_file:
			self.warning.text = 'Please input Message'

		elif not self.message.text and not self.image_file:
			self.warning.text = 'Please input Image and Message'
		
		else:
			if imghdr.what(self.image_path):
				print 'okay' # correct image
				print global_user
				if self.password.text:
					stego.encrypt_image(img_path=self.image_path, key=self.password.text,\
															msg=self.message.text)
					if self.image_path.endswith('.png'):
						new_path = self.image_path
					else:
						new_path = self.image_path + '.png'
					if self.receiver.text:
						print 'new path = ', new_path
						if send(path=new_path, sender=global_user,\
													receiver=self.receiver.text):
							self.warning.text = 'Image has been sent'
							return True
						else:
							self.warning.text = 'Network Error'
					else:
						self.warning.text = "Please add receiver's name"
				else:
					self.warning.text = 'Please input password'
			else:
				self.warning.text = 'Only Image is accepted.'


class View(Screen):

	loadfile = ObjectProperty(None)
	savefile = ObjectProperty(None)
	image_file = None
	image_path = None
	warning = ObjectProperty()
	message = ObjectProperty()
	receiver = ObjectProperty()
	password = ObjectProperty()
	
	def dismiss_popup(self):
		self._popup.dismiss()

	def show_load(self):
		content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
		self._popup = Popup(title="Load Image", content=content,
							size_hint=(0.9, 0.9))
		self._popup.open()

	def load(self, path, filename):
		try:
			with open(os.path.join(path, filename[0])) as stream:
				self.image_file = stream
				print stream
				global global_image_path
				global_image_path = self.image_path = os.path.join(path, filename[0])
				print self.image_path
				#self.image_file = True
		except IndexError:
			self.warning.text = "You've not choose yet"

		self.ids.displayimage.source = self.image_path
		
		self.dismiss_popup()

	def decrypt(self):
		'''
		1. Online:
			User input receiver name to download file
			And then input password
		2. Offline:
			User pick file from his computer
			and then input password
		'''
		#print repr(self.receiver.text), type(self.receiver.text), self.receiver.text ==''
		#print repr(self.image_path), type(self.image_path), self.image_path ==''
		if self.receiver.text or self.image_path:
			print repr(self.receiver.text), type(self.receiver.text)
			print repr(self.image_path), type(self.image_path)
			print bool(self.image_path), bool(self.receiver.text)

			if self.password.text:
				if self.image_path and not self.receiver.text:   # offline case
					print 'offline file, ============'
					#Pick offline file and decrypt
					msg = stego.decrypt_image(key=self.password.text, img_path=self.image_path)
					#print repr(msg)
					self.message.text = 'Decrypted: [' +check_printable(msg) + ']'
				elif self.image_path == None and self.receiver.text != '': #online case
					print 'Get Image Online'
					picture_path = getfile(sender=self.receiver.text, receiver=global_user)
					if picture_path == False:
						self.warning.text = 'Not found Image'
					else:
						msg = stego.decrypt_image(key=self.password.text, img_path=picture_path)
						self.message.text = 'Decrypted: [' +check_printable(msg) + ']'
						self.ids.displayimage.source = picture_path
				elif self.image_path and self.receiver.text:	# both online and offline case, choose offline case
					print 'Both case'
					msg = stego.decrypt_image(key=self.password.text, img_path=self.image_path)
					print repr(msg)
					self.message.text = check_printable(msg)
					

			else:
				self.warning.text = 'Please input password'
		else:
			self.warning.text = 'Pick a file or enter receiver name to download'


class Contacts(Screen):
	pass


class SenareApp(App):
	pass




LabelBase.register(name = 'Roboto',
					fn_regular = 'fonts/Roboto-Light.ttf',
					fn_bold = 'fonts/Roboto-Medium.ttf',
					fn_italic = 'fonts/Roboto-LightItalic.ttf')	

LabelBase.register(name = 'ModernPic',
					fn_regular = 'fonts/modernpics.ttf')
					

if __name__ == '__main__':
	Window.clearcolor = get_color_from_hex('#f0ebce')
	SenareApp().run()