from PIL import Image
from Crypto.Cipher import AES
from Crypto import Random
from hashlib import sha256, md5

ROUND = 500


def a2bits(chars):
	"""
	Convert a string to its bits representation as a string of 0's and 1's.
	"""
	return bin(reduce(lambda x, y : (x<<8)+y, (ord(c) for c in chars), 1))[3:]

def bs(s):
	"""
	Convert a int to its bits representation as a string of 0's and 1's.
	"""
	return str(s) if s<=1 else bs(s>>1) + str(s&1)


def encode_image(img, message):
	"""
	Hide a message (string) in an image use LSB method
	"""
	img = Image.open(img)
	#print img
	encoded = img.copy()
	width, height = img.size
	index = 0
	
	message = message + '~~~'
	message_bits = a2bits(message)
	
	for row in range(height):
		for col in range(width):

			if index + 3 <= len(message_bits) :

				(r, g, b) = img.getpixel((col, row))
					
				# Convert in to bits
				r_bits = bs(r)
				g_bits = bs(g)
				b_bits = bs(b)

				# Replace (in a list) the least significant bit
				# by the bit of the message to hide
				list_r_bits = list(r_bits)
				list_g_bits = list(g_bits)
				list_b_bits = list(b_bits)
				list_r_bits[-1] = message_bits[index]
				list_g_bits[-1] = message_bits[index + 1]
				list_b_bits[-1] = message_bits[index + 2]

				# Convert lists to a strings
				r_bits = "".join(list_r_bits)
				g_bits = "".join(list_g_bits)
				b_bits = "".join(list_b_bits)
				
				# Convert strings of bits to int
				r = int(r_bits, 2)
				g = int(g_bits, 2)
				b = int(b_bits, 2)

				# Save the new pixel
				encoded.putpixel((col, row), (r, g , b))

			index += 3

	return encoded


def decode_image(img):
	"""
	Find a message in an encoded image (with the
	LSB technic).
	"""
	#img = Image.open(img)
	width, height = img.size
	bits = ""
	index = 0
	for row in range(height):
		for col in range(width):
			r, g, b = img.getpixel((col, row))

			bits += bs(r)[-1] + bs(g)[-1] + bs(b)[-1]

			if len(bits) >= 8:
				if chr(int(bits[-8:], 2)) == '~':
					list_of_string_bits = ["".join(list(bits[i*8:(i*8)+8])) for i in range(0, len(bits)/8)]

					list_of_character = [chr(int(elem, 2)) for elem in list_of_string_bits]
					return "".join(list_of_character)[:-1]
	return ""

def padding(key):
	for i in range(ROUND):
		key = sha256(key).digest()
	return key

def encrypt_image(key, img_path, msg):
	pad_key = padding(key)

	for i in range(10):
		try:
			iv = Random.new().read(32)
			iv = md5(iv).digest()
			#print len(iv), repr(iv)
			encryptor = AES.new(pad_key, AES.MODE_CFB, iv)
			#print i
			## Encrypt message before encode to image
			msg_enc = iv + encryptor.encrypt(msg)
			## Encode msg_enc to image
			img_enc = encode_image(img_path, msg_enc)
			temp = decrypt(key=key, img=img_enc)
			if temp == msg:
				if img_path.endswith('.png'):
					img_enc.save(img_path)
					print 'endswith png'
				else:
					img_enc.save(img_path + '.png')
					print 'add png'
				return True
			else:
				print 'Wrong'
				print repr(temp), msg
		except ValueError:
			print 'passssssssssss'
			pass

def decrypt(key, img):
	key = padding(key)
	msg_enc = decode_image(img)
	iv = msg_enc[:16]
	decryptor = AES.new(key, AES.MODE_CFB, iv)
	msg = decryptor.decrypt(msg_enc[16:])
	return msg


def decrypt_image(key, img_path):
	key = padding(key)
	## Decode to get msg_enc
	msg_enc = decode_image(Image.open(img_path))
	## Decrypt message to original
	iv = msg_enc[:16]
	decryptor = AES.new(key, AES.MODE_CFB, iv)
	msg = decryptor.decrypt(msg_enc[16:-1])
	return msg



